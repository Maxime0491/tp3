package ca.ulaval.ima.tp3.offres.classes

import android.os.Parcel
import android.os.Parcelable

class BrandParser (val id: Int, val brandName: String): Parcelable {
    companion object {
        @JvmField
        val CREATOR = object : Parcelable.Creator<BrandParser> {
            override fun createFromParcel(parcel: Parcel) = BrandParser(parcel)
            override fun newArray(size: Int) = arrayOfNulls<BrandParser>(size)
        }
    }
    constructor(parcel: Parcel) : this(
        id = parcel.readInt(),
        brandName = parcel.readString() ?: ""
    )
    override fun describeContents(): Int {
        return 0
    }
    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(brandName)
    }
}