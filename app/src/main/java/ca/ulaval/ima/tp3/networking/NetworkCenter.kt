package ca.ulaval.ima.tp3.networking

import okhttp3.*
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object NetworkCenter {
    private val client = OkHttpClient.Builder().build()

    private val retrofit = Retrofit.Builder()
        .baseUrl(CarAPI.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .client(client)
        .build()

    fun<T> buildServce(service: Class<T>): T{
        return retrofit.create(service)
    }

    init {

    }
}
