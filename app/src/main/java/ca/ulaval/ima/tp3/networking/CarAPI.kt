package ca.ulaval.ima.tp3.networking

import ca.ulaval.ima.tp3.BuildConfig
import ca.ulaval.ima.tp3.offres.classes.Brand
import ca.ulaval.ima.tp3.offres.classes.Model
import ca.ulaval.ima.tp3.offres.classes.PaginatedResultSerializer

import com.google.gson.annotations.SerializedName
import retrofit2.Call
import retrofit2.http.*

interface CarAPI {
    companion object{
        const val API_V1 = "/api/v1/"
        const val BASE_URL = BuildConfig.BASE_URL
    }

    @GET(API_V1 + "brand/")
    fun listBrand() : Call<ContentResponse<ArrayList<Brand>>>

    @GET(API_V1 + "brand/{id}/models")
    fun listModel(@Path("id") id: Int): Call<ContentResponse<ArrayList<Model>>>

    data class ContentResponse<T> (
        @SerializedName("content") val content : T,
        @SerializedName("meta") val meta: Meta,
        @SerializedName("error") var error: Error
   )

    data class Meta (
        @SerializedName("status_code") val statutCode: Int
    )

    data class Error (
        @SerializedName("display") val displayMessage: String
    )


}