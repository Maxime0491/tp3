package ca.ulaval.ima.tp3.offres.fragments

import android.os.Bundle
import android.os.Parcelable
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ca.ulaval.ima.tp3.Communicator
import ca.ulaval.ima.tp3.networking.CarAPI
import ca.ulaval.ima.tp3.offres.classes.Brand
import ca.ulaval.ima.tp3.offres.adapter.BrandRecyclerViewAdapter
import ca.ulaval.ima.tp3.offres.classes.Model

import ca.ulaval.ima.tp3.R
import ca.ulaval.ima.tp3.networking.NetworkCenter
import ca.ulaval.ima.tp3.offres.classes.BrandParser
import com.google.gson.GsonBuilder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class BrandFragment : Fragment() {

    private lateinit var communicator: Communicator

    private lateinit var recycleOffre: RecyclerView
    private lateinit var adapter: BrandRecyclerViewAdapter

    val carNetworkCenter = NetworkCenter.buildServce(CarAPI::class.java)

    var ListeBrand = ArrayList<Brand>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        ListeBrand = getListBrand()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view =  inflater.inflate(R.layout.fragment_offres, container, false)

        communicator = activity as Communicator
        recycleOffre = view.findViewById(R.id.recyclerview_offre)
        recycleOffre.layoutManager = LinearLayoutManager(activity)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }

    fun getListBrand(): ArrayList<Brand> {
        carNetworkCenter.listBrand().enqueue(object :
           Callback<CarAPI.ContentResponse<ArrayList<Brand>>>{
            override fun onResponse(
                call : Call<CarAPI.ContentResponse<ArrayList<Brand>>>,
                response: Response<CarAPI.ContentResponse<ArrayList<Brand>>>
            ){
                if(response.isSuccessful) {
                    response?.body()?.content?.let {
                        for (brand in it) {
                            ListeBrand.add(brand)
                        }
                        Log.d("testouput", "$ListeBrand")
                        activity?.runOnUiThread {

                            adapter = BrandRecyclerViewAdapter(ListeBrand)


                            adapter.setOnBrandClickListerner{
                                val brandPaser = BrandParser(it.id, it.name)
                                communicator.passDataListModel(brandPaser)
                            }

                            recycleOffre.adapter = adapter
                            Log.d("listmarq2","$ListeBrand")

                        }
                    }
                }else{
                    Log.d("Tp3", "Error")
                }
            }

            override fun onFailure(
                call: Call<CarAPI.ContentResponse<ArrayList<Brand>>>,
                t: Throwable
            ) {
                Log.d("Tp3", "Aucun model pour cette marque $t")
            }
        })

        return ListeBrand
    }

}