package ca.ulaval.ima.tp3.offres.fragments

import android.os.Bundle
import android.os.Parcelable
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.ContentView
import androidx.recyclerview.widget.RecyclerView
import ca.ulaval.ima.tp3.Communicator
import ca.ulaval.ima.tp3.R
import ca.ulaval.ima.tp3.networking.CarAPI
import ca.ulaval.ima.tp3.networking.NetworkCenter
import ca.ulaval.ima.tp3.offres.adapter.BrandRecyclerViewAdapter
import ca.ulaval.ima.tp3.offres.adapter.ModelRecyclerViewAdapter
import ca.ulaval.ima.tp3.offres.classes.BrandParser
import ca.ulaval.ima.tp3.offres.classes.Model
import com.google.gson.GsonBuilder
import okhttp3.*
import java.io.IOException


private const val ARG_PARAM1 = "paramlistemodel"

class modellFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private lateinit var brandParser: BrandParser

    private lateinit var communicator: Communicator
    private lateinit var recycleModel: RecyclerView
    private lateinit var adapter: ModelRecyclerViewAdapter
    var Listmodel = ArrayList<Model>()
    val carNetworkCenter = NetworkCenter.buildServce(CarAPI::class.java)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            brandParser = it.getParcelable(ARG_PARAM1)!!
            Log.d("testsendmodel", "$brandParser")

            val idBrand = brandParser.id
            val url_api: String = "https://tp3.infomobile.app/api/v1/brand/$idBrand/models"
            val client = OkHttpClient();
            val request: Request = Request.Builder().url(url_api).build()
            client.newCall(request).enqueue(object : Callback{
                override fun onFailure(call: Call, e: IOException)
                {
                    Log.d("demo", "OnFailure $e")
                }
                override fun onResponse(call: Call?, response: Response?)
                {
                    if (response?.isSuccessful!!) {
                        var list = ArrayList<Model>()
                        val body = response?.body()?.string()
                        val gson = GsonBuilder().create()
                        var resultModels = gson.fromJson(body, ContentModel::class.java)
                        resultModels.content.forEach{
                            Listmodel.add(it)
                            list.add(it)
                        }
                        activity?.runOnUiThread {
                            setAdapter()
                        }

                    } else {
                        println("no_ un")
                    }
                }
            })

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_model, container, false)
        recycleModel = view.findViewById(R.id.framelayout)
        return view
    }

    fun setAdapter(){
        adapter = ModelRecyclerViewAdapter(Listmodel)
        Log.d("listma2","$Listmodel")
        recycleModel.adapter = adapter
    }
}

data  class ContentModel(val content : List<Model>)